import {Platform} from 'react-native';

const Employee = [
    { name: 'Khanh Ha', edu: 'Hang Hai' },
    { name: 'Anh Tuan', edu: 'Viettronics' },
    { name: 'Trong Tuan', edu: 'THCS An Tho' }
];
export const getEmployee = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            return resolve(Employee);
        }, 1000)
    })
}
export const getMovie = () => {
    return fetch('https://facebook.github.io/react-native/movies.json');
}
export const getGames = (start, limit) => {
    return fetch('http://api.gameportal.myg.vn/api/games', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            os: Platform.select({ ios: 'ios', android: 'android' }),
            start:start,
            limit: limit,
        })
    })
}