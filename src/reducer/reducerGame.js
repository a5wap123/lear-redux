import {FETCH_GAME_OK,FETCH_GAME_FAIL,FETCHING_GAME} from '../actions/types';

const DEFAULT_STATE ={
    gameState:[],
    eMgs: '',
    isGameFetching: false,
    isGameFetched: false,
    GameError: false
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case FETCHING_GAME:
            return {
                ...state,
                isGameFetching: true
            };
            case FETCH_GAME_OK:
            return{
                ...state,
                gameState: action.payload,
                isGameFetched: true,
                isGameFetching: false
            };
            case FETCH_GAME_FAIL:
            return { 
                ...state,
                isGameFetching: false,
                error: true,
                GameError: action.eMgs
                
            };
        default:
            return state;
    }
};