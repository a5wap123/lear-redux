import {combineReducers} from 'redux';
import fetchReducer from './fetchReducer';
import reducerGame from './reducerGame';
import reducerMovie from './reducerMovie';
export default combineReducers({
    fetchReducer,
    reducerGame,
    reducerMovie
});