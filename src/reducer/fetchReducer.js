import {FETCH_OK,FETCH_FAIL,FETCHING} from '../actions/types';

const DEFAULT_STATE ={
    dataState:[],
    eMgs: '',
    isFetching: false,
    isFetched: false,
    error: false
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case FETCHING:
            return {
                ...state,
                isFetching: true
            };
            case FETCH_OK:
            return{
                ...state,
                dataState: action.payload,
                isFetched: true,
                isFetching: false
            };
            case FETCH_FAIL:
            return { 
                ...state,
                isFetching: false,
                error: true,
                eMgs: action.eMgs
                
            };
        default:
            return state;
    }
};