import {FETCH_MOVIE_OK,FETCH_MOVIE_FAIL,FETCHING_MOVIE} from '../actions/types';

const DEFAULT_STATE ={
    MovieState:[],
    eMgs: '',
    isMovieFetching: false,
    isMovieFetched: false,
    MovieError: false
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case FETCHING_MOVIE:
            return {
                ...state,
                isMovieFetching: true
            };
            case FETCH_MOVIE_OK:
            return{
                ...state,
                MovieState: action.payload,
                isMOVIEFetched: true,
                isMovieFetching: false
            };
            case FETCH_MOVIE_FAIL:
            return { 
                ...state,
                isMovieFetching: false,
                error: true,
                MovieError: action.eMgs
                
            };
        default:
            return state;
    }
};