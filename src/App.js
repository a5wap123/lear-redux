//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {Provider} from 'react-redux';
import store from './configStore';
import Employee from './components/Employee';
import Movie from './components/Movie';
import Game from './components/Games';
// create a component
export default class App extends Component {
    render() {
        return (
            <Provider store={store} >
                <View style={styles.container}>
                <Game/>
                <Movie/>
                <Employee/>
            </View>
            </Provider>
            
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});
