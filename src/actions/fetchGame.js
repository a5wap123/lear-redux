import { getGames } from '../api/api';
import { FETCH_GAME_OK, FETCH_GAME_FAIL, FETCHING_GAME } from './types';
export const getGame = () => {
    return {
        type: FETCHING_GAME
    }
}
export const getGameSuccess = (data) => {
    return {
        type: FETCH_GAME_OK,
        payload: data
    }
}
export const getGameFail = (eMgs) => {
    return {
        type: FETCH_GAME_FAIL,
        eMgs: eMgs
    }
}
export const fetchGame = (start,limit) => {
    return (dispatch) => {
        dispatch(getGame());
        getGames(start,limit).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.Code == 1) {
                    dispatch(getGameSuccess(responseJson.Data));
                }
                else{
                    getGameFail(responseJson.Message);
                }

            }).catch((e) => {
                getGameFail(e);
            });
    }
}
