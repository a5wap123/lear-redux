export const FETCH_OK = 'FETCH_OK';
export const FETCH_FAIL = 'FETCH_FAIL';
export const FETCHING = 'FETCHING';

//Game
export const FETCH_GAME_OK = 'FETCH_GAME_OK';
export const FETCH_GAME_FAIL = 'FETCH_GAME_FAIL';
export const FETCHING_GAME = 'FETCHING_GAME';

// Movie
//Game
export const FETCH_MOVE_OK = 'FETCH_MOVE_OK';
export const FETCH_MOVIE_FAIL = 'FETCH_MOVIE_FAIL';
export const FETCHING_MOVIE = 'FETCHING_MOVIE';