import { FETCH_OK, FETCH_FAIL, FETCHING } from './types';
import {getEmployee} from '../api/api';

export const getData = () => {
    return {
        type: FETCHING
    }
}
export const getDataSuccess = (data) => {
    return {
        type: FETCH_OK,
        payload: data
    }
}
export const getDataFail = (eMgs) => {
    return {
        type: FETCH_FAIL,
        eMgs: eMgs
    }
}

export const fetchData = () => {
    return (dispatch) => {
        dispatch(getData());
        getEmployee().then((data)=>{
            dispatch(getDataSuccess(data));
        }).catch((e)=>{
            getDataFail(e);
        })
    }
}