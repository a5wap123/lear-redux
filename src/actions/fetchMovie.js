import {getMovie} from '../api/api';
import { FETCH_MOVIE_OK, FETCH_MOVIE_FAIL, FETCHING_MOVIE } from './types';
export const getDataMovie = () => {
    return {
        type: FETCHING_MOVIE
    }
}
export const getMovieSuccess = (data) => {
    return {
        type: FETCH_MOVIE_OK,
        payload: data
    }
}
export const getMovieFail = (eMgs) => {
    return {
        type: FETCH_MOVIE_FAIL,
        eMgs: eMgs
    }
}
export const fetchMovie = () => {
    return (dispatch) => {
        dispatch(getDataMovie());
        getMovie().then((res)=>res.json())
        .then((resJson)=> {
            dispatch(getMovieSuccess(resJson.movies));
        }).catch((e)=>{
            getMovieFail(e)
        });
    }
}
