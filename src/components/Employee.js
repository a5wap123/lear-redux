//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,Button } from 'react-native';
import {connect} from 'react-redux';
import {fetchData} from '../actions';
// create a component
class Employee extends Component {
    render() {
        console.log(this.props)
        const {isFetching,error,eMgs,dataState} = this.props.dataEmployee;
        return (
            <View style={styles.container}>
                <Button title='click me' onPress={
                    ()=>{this.props.fetchData()}
                }/>
                {
                    isFetching && <Text>Loading..</Text>
                    
                }
                {
                    error && <Text>{eMgs}</Text>
                }
                {
                    dataState.length ? (
                        dataState.map((e,i)=>{
                            return(
                                <Text key={i}>Name: {e.name} - {e.edu}</Text>
                            )
                        })
                    ):null
                }
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
const mapStateToProps = (state) => {
    return{
        dataEmployee: state.fetchReducer
    }
}
//make this component available to the app
export default connect(mapStateToProps,{fetchData})(Employee);
