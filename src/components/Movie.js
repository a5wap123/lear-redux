//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,Button } from 'react-native';
import { connect } from 'react-redux';
import { fetchMovie } from '../actions';
// create a component
class Movie extends Component {
    render() {
       console.log(this.props)
        const {isMovieFetching,MovieError,eMgs,MovieState} = this.props.dataMove;
        return (
            <View style={styles.container}>
                <Button title='Get Movie' onPress={
                    ()=>{this.props.fetchMovie()}
                }/>
                {
                    isMovieFetching && <Text>Loading..</Text>
                    
                }
                {
                    MovieError && <Text>{eMgs}</Text>
                }
                {
                    MovieState.length ? (
                        dataState.map((e,i)=>{
                            return(
                                <Text key={i}>Title: {e.title} - {e.releaseYear}</Text>
                            )
                        })
                    ):null
                }
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});
const mapStateToProps = (state) => {
    return {
        dataMove: state.reducerMovie
    }
}
//make this component available to the app
export default connect(mapStateToProps,{fetchMovie})(Movie);
