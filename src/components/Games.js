//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button,ListView,TouchableHighlight,Alert } from 'react-native';
import { connect } from 'react-redux';
import { fetchGame } from '../actions';
// create a component
class Games extends Component {
    render() {
        console.log(this.props)
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        const {isGameFetching, isGameFetched, GameError, eMgs, gameState} = this.props.dataGame;
        return (
            <View style={styles.container}>
                <Button title='Get Game' onPress={
                    () => { this.props.fetchGame(0,5) }
                } />
                {
                    isGameFetching && <Text>Loading..</Text>

                }
                {
                    GameError && <Text>{eMgs}</Text>
                }
                {
                    
                    isGameFetched && 
                    <View style={{ flex: 1, paddingTop: 20, alignItems: 'center' }}>
                        <ListView
                            dataSource={ds.cloneWithRows(gameState)}
                            renderRow={(rowData) =>
                                <View style={{ padding: 5 }}>
                                    <Text>{rowData.Name} - {rowData.Size}, {rowData.Category}</Text>
                                    <TouchableHighlight style={{ padding: 5, backgroundColor: 'green', borderRadius: 10 }}
                                        onPress={() => {
                                            Alert.alert(
                                                `${rowData.Name}`,
                                                `${rowData.UrlFile}`, [
                                                    { text: 'Cancel' },
                                                    { text: 'OK' }
                                                ]
                                            )
                                        }}
                                    >
                                        <Text style={{ textAlign: 'center' }}>Tải game</Text>
                                    </TouchableHighlight>
                                </View>
                            }
                        />
                    </View>
                }
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});
const mapStateToProp = (state) => {
    return {
        dataGame: state.reducerGame
    }
}
//make this component available to the app
export default connect(mapStateToProp, { fetchGame })(Games);
